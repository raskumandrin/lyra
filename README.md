There is a form on a website that submits to a cgi script. The form sends using POST, and I want to run the post multiple times using perl.

I have done this in the past, but it seems that the cgi script sends somewhere else as I can't seem to get it to work.

When you apply I will send link to website for you to tell me if you think you can do it.

Website is http://www.cbs.dtu.dk/services/LYRA/ -

Chain1 and chain2 automate so can get results many times - please see if you can do before continuing.

I would like a Perl script that submits a sequence of letters to chain1 and chain2 and can pull out the data on the other end. To see what sample data is submitted you can see a load sample data: TCR. I will want to submit these letters using perl.

I then want to extract the PDB file from the output page (found at the download PDB file button on the success page).

It should have the PDB file downloaded and avilable for manipulation at the end.