#!/usr/bin/perl

use strict;

use FindBin;
use lib "$FindBin::Bin/lib";

use Mojo::DOM;
use Mojo::UserAgent;

use feature 'say';

my $ua = Mojo::UserAgent->new;

#say $ua->get('https://ya.ru')->res->body;


my $tx = $ua->post(
		'http://www.cbs.dtu.dk/cgi-bin/webface2.fcgi'
		=> {Accept => 'text/html'}
		=> form
		=> {
            'configfile' => '/usr/opt/www/pub/CBS/services/LYRA-1.0/lyra.cf',
			'chain1' => 'GDSVIQMQGQVTFSENDSLFINCTYSTTGYPTLFWYVQYSGEGPQLLLQVTTANNKGSSRGFEATYDKGTTSFHLQKTSVQEIDSAVYYCAISDLSGGSNAKLAFGKGTKLSVK',
			'chain2' => 'ITQTPKFLIGQEGQKLTLKCQQNFNHDTMYWYRQDSGKGLRLIYYSITENDLQKGDLSEGYDASREKKSSFSLTVTSAQKNEMTVFLCASSIRLASAETLYFGSGTRLTVL',
            'blacklistPDBs' => '',
		}
	);
    
say $tx->res->code;
say $tx->res->headers->location; # /cgi-bin/webface2.fcgi?jobid=56AB9F470000557DD9FA2CFC&wait=20

my ($hash) = ( $tx->res->headers->location =~ /jobid=(\w+)/ );

say 'Waiting 4 seconds for response...';
sleep(4);

for(1..10) {
    my $tx = $ua->get("http://www.cbs.dtu.dk/services/LYRA-1.0/tmp/${hash}/lyra_${hash}_refined.pdb");
    say $tx->res->code;
    if ($tx->res->code == 200) {
        open my $fh,'>','lyra.txt';
        print $fh $tx->res->body;
        close $fh;
        say 'result was saved';
        last;
    }
    say 'Waiting one more second for response...';
    sleep(1);
}

# http://www.cbs.dtu.dk/services/LYRA-1.0/tmp/56AB9B1F00005580D92009B2/lyra_56AB9B1F00005580D92009B2_refined.pdb