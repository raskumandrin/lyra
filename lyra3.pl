#!/usr/bin/perl

my $chain1   = 'GDSVIQMQGQVTFSENDSLFINCTYSTTGYPTLFWYVQYSGEGPQLLLQVTTANNKGSSRGFEATYDKGTTSFHLQKTSVQEIDSAVYYCAISDLSGGSNAKLAFGKGTKLSVK';
my $chain2   = 'ITQTPKFLIGQEGQKLTLKCQQNFNHDTMYWYRQDSGKGLRLIYYSITENDLQKGDLSEGYDASREKKSSFSLTVTSAQKNEMTVFLCASSIRLASAETLYFGSGTRLTVL';

use LWP::UserAgent;
 
my $ua = LWP::UserAgent->new;

print "Send POST request\n";

my $response = $ua->post( 'http://www.cbs.dtu.dk/cgi-bin/webface2.fcgi', {
    configfile => '/usr/opt/www/pub/CBS/services/LYRA-1.0/lyra.cf',
	chain1 => $chain1,
	chain2 => $chain2,
    blacklistPDBs => '',
    });

print "Decoding their database hash id from response...\n";

my ($hash) = ( $response->header('Location') =~ /jobid=(\w+)/ );
print "Hash id: $hash\n";

print "Waiting 6 seconds for generation results on server...\n";
sleep(6);

for(1..16) {
    my $res = $ua->get("http://www.cbs.dtu.dk/services/LYRA-1.0/tmp/${hash}/lyra_${hash}_refined.pdb");
    if ($res->is_success) {
        open my $fh,'>',"$hash.pdb" or die "Cannot open file $hash.pdb for writing: $!";
        print $fh $res->content;
        close $fh;
        print "Data saved in $hash.pdb\n";
        last;
    }
    else {
        print "Waiting one more second...\n";
        sleep(1);
    }
}
